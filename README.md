# update-resolv-conf for OpenVPN
![](https://github.com/nu11secur1ty/update-resolv-conf-for-OpenVPN-in-OpenSUSE-Leap-15-42.2-42.3/blob/master/openvpntech_logo_rounded_antialiased.png)
# nu11secur1ty: deploying for OpenSUSE Leap 15, 42.2, 42.3
Helper script for OpenVPN to directly update the DNS settings of a link through systemd-resolved via DBus.
![img](https://github.com/nu11secur1ty/update-systemd-resolved/blob/master/openvpntech_logo_rounded_antialiased.png)


# Installation:

- 1
```bash
git clone https://github.com/nu11secur1ty/update-resolv-conf-for-OpenVPN-in-OpenSUSE-Leap-15-42.2-42.3.git && cd update-resolv-conf-for-OpenVPN-in-OpenSUSE-Leap-15-42.2-42.3
```
- 2
```bash
mv client/ server/ update-resolv-conf /etc/openvpn/
```
- 3
```bash
# Check
ls /etc/openvpn/
```

# Good luck...;)
