# Packages
```bash
apt install -y network-manager-openconnect-gnome
```
# Usage

- .Select the Cisco AnyConnect VPN Protocol
- .Enter your VPN address into the Gateway text box
- .Save the VPN Settings
- .Activate the VPN
- .Follow the prompt for your user credentials

# BR
